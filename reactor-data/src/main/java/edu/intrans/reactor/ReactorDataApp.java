package edu.intrans.reactor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@SpringBootApplication(scanBasePackages = "edu.intrans.reactor.*")
@ComponentScan(basePackages = "edu.intrans.reactor.*")
@Import({ KafkaConfiguration.class })
@EnableScheduling
public class ReactorDataApp {

	public static void main(String[] args) {
		SpringApplication.run(ReactorDataApp.class, args);
	}

	@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER })
	@Retention(RetentionPolicy.RUNTIME)
	@Qualifier
	public static @interface DataSource {
		Type value();

		public static enum Type {
			SENSOR_DATA, SENSOR_INVENTORY, INRIX, WAZE, SNOWPLOW;
		}
	}

	/**
	 * Customized {@link RestTemplate} with XML Binding options.
	 * 
	 * @return {@link RestTemplate}
	 */
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		for (int i = 0; i < restTemplate.getMessageConverters().size(); i++) {
			if (restTemplate.getMessageConverters().get(i) instanceof MappingJackson2XmlHttpMessageConverter) {
				restTemplate.getMessageConverters().set(i, mappingJackson2XmlHttpMessageConverter());
			}
		}
		return restTemplate;
	}

	/**
	 * Modified XML Mapper.
	 * 
	 * @return {@link MappingJackson2XmlHttpMessageConverter}
	 */
	@Bean
	public MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter() {
		MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter = new MappingJackson2XmlHttpMessageConverter();
		XmlMapper mapper = new XmlMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mappingJackson2XmlHttpMessageConverter.setObjectMapper(mapper);
		return mappingJackson2XmlHttpMessageConverter;
	}

	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(5);
		taskExecutor.setMaxPoolSize(10);
		return taskExecutor;
	}

}
