package edu.intrans.reactor.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("datasource")
public class DataSourceProperties {
	private final SensorData sensorData = new SensorData();

	public SensorData getSensorData() {
		return sensorData;
	}

	public static class SensorData {
		private String baseURL;
		private String qParam;
		private String cron;

		public String getBaseURL() {
			return baseURL;
		}

		public void setBaseURL(String baseURL) {
			this.baseURL = baseURL;
		}

		public String getqParam() {
			return qParam;
		}

		public void setqParam(String qParam) {
			this.qParam = qParam;
		}

		public String getCron() {
			return cron;
		}

		public void setCron(String cron) {
			this.cron = cron;
		}
	}

}
