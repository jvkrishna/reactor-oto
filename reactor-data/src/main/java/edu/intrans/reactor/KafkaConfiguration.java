package edu.intrans.reactor;

import static edu.intrans.reactor.constants.KafkaConstants.BOOTSTRAP_SERVERS;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerializer;

import edu.intrans.reactor.ReactorDataApp.DataSource;
import edu.intrans.reactor.data.dto.SensorDataDTO;

@Configuration
public class KafkaConfiguration {

//	@Bean
//	public KafkaAdmin kafkaAdmin() {
//		Map<String, Object> configs = new HashMap<>();
//		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
//		return new KafkaAdmin(configs);
//	}

	@Bean
	@DataSource(DataSource.Type.SENSOR_DATA)
	public KafkaTemplate<String, SensorDataDTO> sensorDataKafkaTemplate() {
		Map<String, Object> props = getDefaultProps();
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 180 * 1000);
		return new KafkaTemplate<>(new DefaultKafkaProducerFactory<String, SensorDataDTO>(props));

	}

	// @Bean
	// public NewTopic sensorDataTopic() {
	// return new NewTopic(SENSOR_DATA_TOPIC, NUM_PARTITIONS, REPLICATION_FACTOR);
	// }

	private Map<String, Object> getDefaultProps() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		return props;

	}

}
