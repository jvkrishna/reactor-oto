package edu.intrans.reactor.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.intrans.reactor.data.process.DataDownloadProcess;

@RestController
@RequestMapping("kakfa")
public class TestController {

	@Autowired
	private DataDownloadProcess dataDownloadProcess;

	@GetMapping("/test/{data}")
	public String post(@PathVariable("data") final String data) throws Exception {
		// kafkaTemplate.send("test", data);
		dataDownloadProcess.sensorDataProcess();
		return "posted";
	}
}
