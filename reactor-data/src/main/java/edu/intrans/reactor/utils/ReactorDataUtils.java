package edu.intrans.reactor.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.intrans.reactor.data.dto.SensorDataDTO;

public class ReactorDataUtils {

	public static List<SensorDataDTO> getSensorDataDTOs(String xmlData)
			throws JsonProcessingException, TransformerException, IOException, ParserConfigurationException, SAXException {
		ClassPathResource styleSheetStream = new ClassPathResource("sensordata.xsl");
		String txData = ReactorUtils.transformXML(xmlData, new StreamSource(styleSheetStream.getInputStream()));
		return CSVUtils.readAsCSV(txData, SensorDataDTO.class, false);
	}

	public static void main(String[] args)
			throws IOException, TransformerException, ParserConfigurationException, SAXException {

		String xmlData = FileUtils.readFileToString(new File("test.xml"), Charset.defaultCharset());
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(xmlData)));
		StreamSource stylesource = new StreamSource(new FileInputStream("sensordata.xsl"));
		Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
		Source source = new StreamSource( new StringReader(xmlData));
		StringWriter writer = new StringWriter();
		Result outputTarget = new StreamResult(writer);
		transformer.transform(new DOMSource(document), outputTarget);
		List<SensorDataDTO> list  = CSVUtils.readAsCSV(writer.toString(),SensorDataDTO.class,false);
		System.out.println(list.size());
	}
	
	public static void convert(File xmlSource, InputStream xslt, File csvDest) throws Exception {
		convert(new FileInputStream(xmlSource), xslt, csvDest);
	}

	public static void convert(InputStream xmlStream, InputStream xslt, File csvDest) throws Exception {
//		assert (xmlStream != null && csvDest != null && xslt != null);
//		Document document = TimeliUtils.parseXML(xmlStream);
//		StreamSource stylesource = new StreamSource(xslt);
//		Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
//		Source source = new DOMSource(document);
//		OutputStream os = new FileOutputStream(csvDest, true);
//		Result outputTarget = new StreamResult(os);
//		transformer.transform(source, outputTarget);
	}
}
