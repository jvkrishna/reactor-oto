package edu.intrans.reactor.data.schedular;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import edu.intrans.reactor.data.process.DataDownloadProcess;

/**
 * Schedular job for data archiving.
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */

@Component
public class DataDownloadJob {
	@Autowired
	private DataDownloadProcess dataDownloadProcess;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Scheduled(cron = "${datasource.sensorData.cron}")
	public void downloadSensorData() {
		taskExecutor.execute(() -> {
			try {
				System.out.println("Started download process for sensor data");
				dataDownloadProcess.sensorDataProcess();
				System.out.println("Completed download process.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

}
