package edu.intrans.reactor.data.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import edu.intrans.reactor.constants.DataSourceProperties;
import edu.intrans.reactor.constants.DataSourceProperties.SensorData;

@Component
public class RestDataConsumer {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private DataSourceProperties dataSourceProperties;

	/**
	 * Download IADOT wavetronix sensor data and return the XML response in String
	 * format.
	 * 
	 * @return XML Sensor Data.
	 */
	public String downloadSensorData() {
		SensorData sensorDataProps = dataSourceProperties.getSensorData();
		String reqMsg = "UseLocationReference";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(sensorDataProps.getBaseURL())
				.queryParam(sensorDataProps.getqParam(), reqMsg);
		return restTemplate.getForObject(uriBuilder.build().encode().toUriString(), String.class);
	}

}
