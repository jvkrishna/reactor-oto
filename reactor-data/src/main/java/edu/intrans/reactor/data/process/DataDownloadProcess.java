package edu.intrans.reactor.data.process;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.intrans.reactor.ReactorDataApp.DataSource;
import edu.intrans.reactor.ReactorDataApp.DataSource.Type;
import edu.intrans.reactor.data.consumer.RestDataConsumer;
import edu.intrans.reactor.data.dto.SensorDataDTO;
import edu.intrans.reactor.kafka.producer.KafkaProducer;
import edu.intrans.reactor.utils.ReactorDataUtils;

@Component
public class DataDownloadProcess {
	@Autowired
	private RestDataConsumer restDataConsumer;

	@DataSource(Type.SENSOR_DATA)
	@Autowired
	private KafkaProducer<String, SensorDataDTO> kafkaProducer;

	public void sensorDataProcess() throws Exception {
		String xmlData = restDataConsumer.downloadSensorData();
		List<SensorDataDTO> sensorData = ReactorDataUtils.getSensorDataDTOs(xmlData);
		kafkaProducer.send(sensorData);
		System.out.println("Published");

	}
}
