package edu.intrans.reactor.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import edu.intrans.reactor.ReactorDataApp.DataSource;
import edu.intrans.reactor.ReactorDataApp.DataSource.Type;
import edu.intrans.reactor.constants.KafkaConstants;
import edu.intrans.reactor.data.dto.SensorDataDTO;
import edu.intrans.reactor.utils.DateUtils;

@DataSource(Type.SENSOR_DATA)
@Component
public class SensorDataKafkaProducer implements KafkaProducer<String, SensorDataDTO> {

	@Autowired
	@DataSource(DataSource.Type.SENSOR_DATA)
	private KafkaTemplate<String, SensorDataDTO> kafkaTemplate;

	@Override
	public ListenableFuture<SendResult<String, SensorDataDTO>> send(SensorDataDTO record) {
		String key = DateUtils.formatDate(DateUtils.getCurrentDate(), DateUtils.PATTERN_MM_DD_YYYY);
		return kafkaTemplate.send(KafkaConstants.SENSOR_DATA_TOPIC, key, record);
	}

}
