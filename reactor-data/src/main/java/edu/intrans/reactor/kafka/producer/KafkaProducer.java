package edu.intrans.reactor.kafka.producer;

import java.util.List;

import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

public interface KafkaProducer<K, V> {

	ListenableFuture<SendResult<K, V>> send(V record);

	default void send(List<V> records) {
		for (V record : records) {
			send(record);
		}
	}

}
