<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="text" omit-xml-declaration="yes" indent="no" />
	<xsl:strip-space elements="*" />
	<xsl:template match="/">
		<!-- <xsl:text>owner-Id,network-Id,local-date,local-time,utc-offset,start-time,end-time,detector-Id,status,lane-Id,lane-count,lane-volume,lane-occupancy,lane-speed,small-class-count,small-class-volume,medium-class-count,medium-class-volume,large-class-count,large-class-volume&#xa;</xsl:text> -->
		<xsl:for-each select="//lane">
			<xsl:variable name="lane" select="."></xsl:variable>
			<xsl:apply-templates select="/trafficDetectorData"></xsl:apply-templates>
			<xsl:text>,</xsl:text>
			<xsl:apply-templates select="//collection-period"></xsl:apply-templates>
			<xsl:text>,</xsl:text>
			<xsl:for-each select="ancestor::detector-report">
				<xsl:apply-templates select="."></xsl:apply-templates>
				<xsl:text>,</xsl:text>
				<xsl:apply-templates select="$lane"></xsl:apply-templates>
				<xsl:text>,</xsl:text>
				<xsl:apply-templates select="$lane/child::classes/class[class-id='Small']"></xsl:apply-templates>
				<xsl:text>,</xsl:text>
				<xsl:apply-templates select="$lane/child::classes/class[class-id='Medium']"></xsl:apply-templates>
				<xsl:text>,</xsl:text>
				<xsl:apply-templates select="$lane/child::classes/class[class-id='Large']"></xsl:apply-templates>
				<xsl:text>&#xa;</xsl:text>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="trafficDetectorData">
		<xsl:value-of select="./owner-id"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="./network-id"></xsl:value-of>
	</xsl:template>

	<xsl:template match="collection-period">
		<xsl:value-of select="./detection-time-stamp/local-date"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="./detection-time-stamp/local-time"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="./detection-time-stamp/utc-offset"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="./start-time"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="./end-time"></xsl:value-of>
	</xsl:template>

	<xsl:template match="detector-report">
		<xsl:value-of select="detector-id"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="status"></xsl:value-of>
	</xsl:template>

	<xsl:template match="lane">
		<xsl:value-of select="lane-id"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="count"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="volume"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="occupancy"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="speed"></xsl:value-of>
	</xsl:template>

	<xsl:template match="class">
		<xsl:value-of select="./count"></xsl:value-of>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="./volume"></xsl:value-of>
	</xsl:template>
</xsl:stylesheet>