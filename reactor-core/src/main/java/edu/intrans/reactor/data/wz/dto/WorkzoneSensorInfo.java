package edu.intrans.reactor.data.wz.dto;

import java.io.Serializable;

public class WorkzoneSensorInfo implements Serializable {

	private static final long serialVersionUID = 5010311302796206072L;

	private String id;
	private String sensorId;
	private String sensorName;
	private double lattitude;
	private double longitude;
	private double linearReference;
	private String route;
	private String direction;
	private int codedDirection;
	private String projectName;
	private String group;
	private String camera;
	private String routeId;
	private double measure;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public double getLattitude() {
		return lattitude;
	}

	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLinearReference() {
		return linearReference;
	}

	public void setLinearReference(double linearReference) {
		this.linearReference = linearReference;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCamera() {
		return camera;
	}

	public void setCamera(String camera) {
		this.camera = camera;
	}

	public String getRouteId() {
		return routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public double getMeasure() {
		return measure;
	}

	public void setMeasure(double measure) {
		this.measure = measure;
	}

	public int getCodedDirection() {
		return codedDirection;
	}

	public void setCodedDirection(int codedDirection) {
		this.codedDirection = codedDirection;
	}

}
