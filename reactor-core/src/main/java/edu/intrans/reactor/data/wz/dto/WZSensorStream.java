package edu.intrans.reactor.data.wz.dto;

import java.io.Serializable;

import edu.intrans.reactor.data.dto.SensorDataDTO;
import edu.intrans.reactor.utils.DateUtils;

public class WZSensorStream implements Serializable {

	private static final long serialVersionUID = 280452764868281650L;
	private String workzone;
	private String sensorId;
	private double latitude;
	private double longitude;
	private String direction;
	private int codedDirection;
	private double linearReference;
	private String cameraId;
	private SensorDataDTO sensorData;
	private long time;

	public WZSensorStream(String workzone, String sensorId, double latitude, double longitude, String direction,
			int codedDirection, double linearReference, String cameraId, SensorDataDTO sensorData) {
		super();
		this.workzone = workzone;
		this.sensorId = sensorId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.direction = direction;
		this.codedDirection = codedDirection;
		this.linearReference = linearReference;
		this.sensorData = sensorData;
		this.cameraId = cameraId;
		this.time = DateUtils.getCurrentTime();
	}

	public String getWorkzone() {
		return workzone;
	}

	public void setWorkzone(String workzone) {
		this.workzone = workzone;
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public SensorDataDTO getSensorData() {
		return sensorData;
	}

	public void setSensorData(SensorDataDTO sensorData) {
		this.sensorData = sensorData;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getCodedDirection() {
		return codedDirection;
	}

	public void setCodedDirection(int codedDirection) {
		this.codedDirection = codedDirection;
	}

	public double getLinearReference() {
		return linearReference;
	}

	public void setLinearReference(double linearReference) {
		this.linearReference = linearReference;
	}

	public String getCameraId() {
		return cameraId;
	}

	public void setCameraId(String cameraId) {
		this.cameraId = cameraId;
	}

}
