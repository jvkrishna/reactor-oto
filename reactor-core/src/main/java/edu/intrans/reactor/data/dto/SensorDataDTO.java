package edu.intrans.reactor.data.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "ownerId", "networkId", "localDate", "localTime", "utcOffset", "startTime", "endTime",
		"detectorId", "status", "laneId", "laneCount", "laneVolume", "laneOccupancy", "laneSpeed", "smallCount",
		"smallVolume", "mediumCount", "mediumVolume", "largeCount", "largeVolume" })
public class SensorDataDTO implements Serializable {

	private static final long serialVersionUID = -6322166995952842041L;
	private String ownerId;
	private String networkId;
	private String localDate;
	private String localTime;
	private String utcOffset;
	private String startTime;
	private String endTime;
	private String detectorId;
	private String status;
	private String laneId;
	private String laneCount;
	private String laneVolume;
	private String laneOccupancy;
	private String laneSpeed;
	private String smallCount;
	private String smallVolume;
	private String mediumCount;
	private String mediumVolume;
	private String largeCount;
	private String largeVolume;

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

	public String getLocalTime() {
		return localTime;
	}

	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}

	public String getUtcOffset() {
		return utcOffset;
	}

	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getDetectorId() {
		return detectorId;
	}

	public void setDetectorId(String detectorId) {
		this.detectorId = detectorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLaneId() {
		return laneId;
	}

	public void setLaneId(String laneId) {
		this.laneId = laneId;
	}

	public String getLaneCount() {
		return laneCount;
	}

	public void setLaneCount(String laneCount) {
		this.laneCount = laneCount;
	}

	public String getLaneVolume() {
		return laneVolume;
	}

	public void setLaneVolume(String laneVolume) {
		this.laneVolume = laneVolume;
	}

	public String getLaneOccupancy() {
		return laneOccupancy;
	}

	public void setLaneOccupancy(String laneOccupancy) {
		this.laneOccupancy = laneOccupancy;
	}

	public String getLaneSpeed() {
		return laneSpeed;
	}

	public void setLaneSpeed(String laneSpeed) {
		this.laneSpeed = laneSpeed;
	}

	public String getSmallCount() {
		return smallCount;
	}

	public void setSmallCount(String smallCount) {
		this.smallCount = smallCount;
	}

	public String getSmallVolume() {
		return smallVolume;
	}

	public void setSmallVolume(String smallVolume) {
		this.smallVolume = smallVolume;
	}

	public String getMediumCount() {
		return mediumCount;
	}

	public void setMediumCount(String mediumCount) {
		this.mediumCount = mediumCount;
	}

	public String getMediumVolume() {
		return mediumVolume;
	}

	public void setMediumVolume(String mediumVolume) {
		this.mediumVolume = mediumVolume;
	}

	public String getLargeCount() {
		return largeCount;
	}

	public void setLargeCount(String largeCount) {
		this.largeCount = largeCount;
	}

	public String getLargeVolume() {
		return largeVolume;
	}

	public void setLargeVolume(String largeVolume) {
		this.largeVolume = largeVolume;
	}

}
