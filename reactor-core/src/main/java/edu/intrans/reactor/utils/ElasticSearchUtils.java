package edu.intrans.reactor.utils;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;

/**
 * Helper methods for ElasticSearch REST queries.
 * 
 * @author krishnaj
 *
 */
public class ElasticSearchUtils {

	public static RestHighLevelClient getDefaultESClient() {
		RestClientBuilder esClientBuilder = RestClient.builder(new HttpHost("reactorfeeds.org", 9200, "http")).setMaxRetryTimeoutMillis(300*1000);
		return new RestHighLevelClient(esClientBuilder);
	}

	public static <T> void bulkIndexToES(List<T> objs, String index, String type) throws IOException {
		bulkIndexToES(getDefaultESClient(), objs, index, type);
	}

	public static <T> void bulkIndexToES(RestHighLevelClient client, List<T> objs, String index, String type)
			throws IOException {
		BulkRequest bulkRequest = new BulkRequest();
		for (T obj : objs) {
			bulkRequest
					.add(new IndexRequest(index, type).source(ReactorUtils.writeValueAsJSON(obj), XContentType.JSON));
		}
		BulkResponse bulkResponse = client.bulk(bulkRequest);
		if (bulkResponse.hasFailures()) {
			for (BulkItemResponse bulkItemResponse : bulkResponse) {
				if (bulkItemResponse.isFailed()) {
					System.out.println(bulkItemResponse.getFailureMessage());
				}
			}
		}
	}

	/**
	 * Fetches all documents with scroll requests.
	 * 
	 * @param client
	 * @param searchRequest
	 * @return
	 * @throws IOException
	 */
	public static List<SearchHit> getAllSearchResults(RestHighLevelClient client, SearchRequest searchRequest)
			throws IOException {
		List<SearchHit> searchHitsList = new LinkedList<>();
		final Scroll scroll = new Scroll(TimeValue.timeValueSeconds(10));
		searchRequest.scroll(scroll);
		SearchResponse searchResponse = client.search(searchRequest);
		// get first batch of response.
		String scrollId = searchResponse.getScrollId();
		SearchHit[] searchHits = searchResponse.getHits().getHits();
		// Repeat request till scroll reaches end.
		while (searchHits != null && searchHits.length > 0) {
			for (SearchHit hit : searchHits) {
				searchHitsList.add(hit);
			}
			SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
			scrollRequest.scroll(scroll);
			searchResponse = client.searchScroll(scrollRequest);
			scrollId = searchResponse.getScrollId();
			searchHits = searchResponse.getHits().getHits();
		}

		// Clear scroll from the request.
		ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
		clearScrollRequest.addScrollId(scrollId);
		client.clearScroll(clearScrollRequest);
		return searchHitsList;

	}
}
