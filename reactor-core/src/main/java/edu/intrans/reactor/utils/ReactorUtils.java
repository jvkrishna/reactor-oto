package edu.intrans.reactor.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

/**
 * Utility Functions.
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Mar 30, 2017
 *
 */
public class ReactorUtils {

	private static ObjectMapper jsonMapper = new ObjectMapper();
	private static ObjectMapper xmlMapper = new XmlMapper();
	static {
		configureObjectMapper(jsonMapper);
		configureObjectMapper(xmlMapper);
	}

	/**
	 * Converts XML content into an Object.
	 * 
	 * @param xmlContent
	 * @param valueType
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	public static <T> T readValue(String xmlContent, Class<T> valueType) throws IOException {
		return readXMLValue(xmlContent, valueType);
	}

	/**
	 * Converts an Object to XML String. use writeValueAsXML instead.
	 * 
	 * @param obj
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	public static <T> String writeValueAsString(T obj) throws IOException {
		return writeValueAsXML(obj);
	}

	public static <T> T readXMLValue(String xmlContent, Class<T> valueType) throws IOException {
		return readValue(xmlMapper, xmlContent, valueType);
	}

	public static <T> String writeValueAsXML(T obj) throws IOException {
		return writeValueAsString(xmlMapper, obj);
	}

	public static <T> T readJSONValue(String jsonContent, Class<T> valueType) throws IOException {
		return readValue(jsonMapper, jsonContent, valueType);
	}

	public static <T> String writeValueAsJSON(T obj) throws IOException {
		return writeValueAsString(jsonMapper, obj);
	}

	public static <T> T readValue(ObjectMapper om, String content, Class<T> valueType) throws IOException {
		return om.readValue(content, valueType);
	}

	public static <T> String writeValueAsString(ObjectMapper om, T obj) throws IOException {
		return om.writeValueAsString(obj);
	}

	/**
	 * Converts an Object to XML string with custom parameters.
	 * 
	 * @param obj
	 * @param inclusions
	 * @return
	 * @throws IOException
	 */
	public static <T> String writeValueAsString(T obj, List<JsonInclude.Include> inclusions) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		for (JsonInclude.Include incl : inclusions) {
			mapper.setSerializationInclusion(incl);
		}
		return mapper.writeValueAsString(obj);
	}

	public static void updateCSVFileWithHeaders(File f, String... headers) throws IOException {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < headers.length - 1; i++) {
			sb.append(headers[i]);
		}
		sb.append(headers[headers.length - 1]).append("\n");
		FileUtils.writeStringToFile(f, sb.toString(), Charset.defaultCharset());
	}

	public static void gUnZip(File gzFile, File output) throws IOException {
		byte[] buffer = new byte[1024];
		GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(gzFile));

		FileOutputStream out = new FileOutputStream(output);

		int len;
		while ((len = gzis.read(buffer)) > 0) {
			out.write(buffer, 0, len);
		}

		gzis.close();
		out.close();

	}

	/**
	 * Parse a given XML string to {@link Document} object
	 * 
	 * @param xmlString
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document parseXML(String xmlString) throws ParserConfigurationException, SAXException, IOException {
		return parseXML(new ByteArrayInputStream(xmlString.getBytes()));
	}

	/**
	 * Parse a given input stream to {@link Document} object
	 * 
	 * @param xmlInputStream
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document parseXML(InputStream xmlInputStream)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(xmlInputStream);

	}

	/**
	 * Creates basic authentication interceptor.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static ClientHttpRequestInterceptor getBasicAuthInterceptor(String username, String password) {
		return new BasicAuthorizationInterceptor(username, password);
	}

	public static String formatStringToDecimal(String decimalString, int decimalLocation) {
		return new BigDecimal(new BigInteger(decimalString), decimalLocation).toString();
	}

	public static String formatLocationString(String str) {
		return formatStringToDecimal(str, 6);
	}

	public static <T> Stream<T> asStream(Iterator<T> sourceItr) {
		return asStream(sourceItr, false);
	}

	public static <T> Stream<T> asStream(Iterator<T> sourceItr, boolean parallel) {
		Iterable<T> iterable = () -> sourceItr;
		return StreamSupport.stream(iterable.spliterator(), parallel);
	}

	public static String transformXML(String xmlData, Source xslSource)
			throws TransformerException, ParserConfigurationException, SAXException, IOException {
		StringReader reader = new StringReader(xmlData);
		StringWriter writer = new StringWriter();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(reader));
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer(xslSource);
		transformer.transform(new DOMSource(document), new StreamResult(writer));
		return writer.toString();
	}

	private static void configureObjectMapper(ObjectMapper om) {
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		om.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
		om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY).setVisibility(PropertyAccessor.GETTER, Visibility.NONE)
				.setVisibility(PropertyAccessor.IS_GETTER, Visibility.NONE);
		om.registerModule(new JaxbAnnotationModule());
	}

}
