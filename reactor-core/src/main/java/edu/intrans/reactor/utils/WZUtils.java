package edu.intrans.reactor.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import edu.intrans.reactor.data.wz.dto.WorkzoneSensorInfo;

/**
 * 
 * @author krishnaj
 *
 */
public class WZUtils {

	public static List<WorkzoneSensorInfo> createWorkzoneSensorInfoFromFile(InputStream wzFileStream)
			throws IOException {
		List<WorkzoneSensorInfo> workzoneSensorInfoList = new ArrayList<>();
		Reader in = new InputStreamReader(wzFileStream);
		Iterator<CSVRecord> records = CSVUtils.parseCSVFileWithHeaders(in).iterator();
		WorkzoneSensorInfo workzoneSensorInfo = null;
		while (records.hasNext()) {
			CSVRecord record = records.next();
			workzoneSensorInfo = new WorkzoneSensorInfo();
			workzoneSensorInfo.setSensorId(record.get("SensorID"));
			workzoneSensorInfo.setSensorName(StringUtils.deleteWhitespace(record.get("SensorName")));
			workzoneSensorInfo.setLattitude(Double.parseDouble(record.get("Latitude")));
			workzoneSensorInfo.setLongitude(Double.parseDouble(record.get("Longitude")));
			workzoneSensorInfo.setLinearReference(Double.parseDouble(record.get("LinearReference")));
			workzoneSensorInfo.setRoute(record.get("Route"));
			workzoneSensorInfo.setDirection(record.get("direction"));
			workzoneSensorInfo.setCodedDirection(Integer.parseInt(record.get("coded_direction")));
			workzoneSensorInfo.setProjectName(record.get("ProjectName"));
			workzoneSensorInfo.setGroup(record.get("Group"));
			workzoneSensorInfo.setCamera(record.get("CameraID"));
			workzoneSensorInfo.setRouteId(record.get("routeId"));
			workzoneSensorInfo.setMeasure(Double.parseDouble(record.get("Measure")));
			workzoneSensorInfoList.add(workzoneSensorInfo);
		}
		return workzoneSensorInfoList;
	}

}
