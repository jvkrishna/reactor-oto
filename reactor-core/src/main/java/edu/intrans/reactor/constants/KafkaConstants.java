package edu.intrans.reactor.constants;

public class KafkaConstants {
	public static final String BOOTSTRAP_SERVERS = "rpm01.intrans.iastate.edu:9092";
	public static final short REPLICATION_FACTOR = 3;
	public static final int NUM_PARTITIONS = 10;

	public static final String SENSOR_DATA_TOPIC = "test";
}
