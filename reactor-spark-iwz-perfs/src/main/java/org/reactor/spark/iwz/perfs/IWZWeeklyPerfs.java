package org.reactor.spark.iwz.perfs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * IWZ Weekly Performance measures
 * 
 * @author krishnaj
 *
 */
public class IWZWeeklyPerfs {

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("iwz-weekly-perfs").setMaster("local[2]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JavaRDD<String> waveFile = sc
				.textFile("hdfs://master.intrans.iastate.edu:8020/user/team/WAVETRONIX/IOWA/2018/201808/01082018.txt");
		System.out.println(waveFile.count());
		sc.close();

	}
}
