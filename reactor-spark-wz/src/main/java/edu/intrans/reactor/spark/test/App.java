package edu.intrans.reactor.spark.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;

import edu.intrans.reactor.constants.KafkaConstants;
import edu.intrans.reactor.data.dto.SensorDataDTO;
import edu.intrans.reactor.data.wz.dto.WZSensorStream;
import edu.intrans.reactor.data.wz.dto.WorkzoneSensorInfo;
import edu.intrans.reactor.utils.ReactorUtils;
import edu.intrans.reactor.utils.WZUtils;

public class App {

	public static void main(String[] args) throws InterruptedException, IOException {

		// Get the workzone sensors meta data.
		List<WorkzoneSensorInfo> wzSensorInfo = WZUtils
				.createWorkzoneSensorInfoFromFile(App.class.getResourceAsStream("/configs/workzoneSensors.csv"));

		Map<String, List<WorkzoneSensorInfo>> wzSensorMap = wzSensorInfo.stream()
				.collect(Collectors.groupingBy(WorkzoneSensorInfo::getSensorId));

		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "rpm01.intrans.iastate.edu:9092");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "fasdfas");
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
		// props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
		// props.put("spring.kafka.consumer.properties.spring.json.value.default.type",
		// "edu.intrans.reactor.data.dto.SensorDataDTO");
		// props.put("spring.kafka.consumer.properties.spring.json.trusted.packages",
		// "*");
		Collection<String> topics = Arrays.asList(KafkaConstants.SENSOR_DATA_TOPIC);

		SparkConf conf = new SparkConf().setAppName("vamsiTest2").setMaster("local[2]");

		conf.set("es.nodes", "reactorfeeds.org:9200");
		conf.set("es.nodes.wan.only", "true");
		// SparkSession spark = SparkSession.builder().config(conf).getOrCreate();
		// JavaSparkContext jsc = new JavaSparkContext(conf);
		JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(20));
		System.out.println("Streaming context created.");

		JavaInputDStream<ConsumerRecord<String, String>> ostream = KafkaUtils.createDirectStream(jssc,
				LocationStrategies.PreferConsistent(), ConsumerStrategies.<String, String>Subscribe(topics, props));
		JavaDStream<SensorDataDTO> stream = ostream
				.map(record -> ReactorUtils.readJSONValue(record.value(), SensorDataDTO.class));
		stream.count().print();
		stream.filter(sd -> wzSensorMap.containsKey(sd.getDetectorId())).foreachRDD((sds, time) -> {
			List<SensorDataDTO> sdsList = sds.collect();
			List<WZSensorStream> wzSensorStream = new ArrayList<>();
			sdsList.forEach(sd -> {
				List<WorkzoneSensorInfo> wzInfos = wzSensorMap.get(sd.getDetectorId());
				for (WorkzoneSensorInfo wzInfo : wzInfos) {
					WZSensorStream wzStream = new WZSensorStream(wzInfo.getGroup(), wzInfo.getSensorId(),
							wzInfo.getLattitude(), wzInfo.getLongitude(), wzInfo.getDirection(),
							wzInfo.getCodedDirection(), wzInfo.getLinearReference(), wzInfo.getCamera(), sd);
					wzSensorStream.add(wzStream);
				}

			});
			// Index the data into elastic search.
			System.out.println(wzSensorStream.size());
			if (!wzSensorStream.isEmpty()) {
				// ElasticSearchUtils.bulkIndexToES(wzSensorStream, "iwz-sensor", "data");
				JavaEsSpark.saveToEs(jssc.sparkContext().parallelize(wzSensorStream), "iwz-sensor/data");
			}
		});

		jssc.start();
		jssc.awaitTermination();

	}
}
